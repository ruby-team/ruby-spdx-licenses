Source: ruby-spdx-licenses
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Gabriel Filion <gabster@lelutin.ca>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby-minitest,
               ruby-mocha
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-spdx-licenses.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-spdx-licenses
Homepage: https://github.com/domcleal/spdx-licenses
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-spdx-licenses
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Multi-Arch: foreign
Description: SPDX license and identifier lookup
 This library provides validation and additional data about SPDX licenses and
 identifiers. It makes it possible to programmatically find out if a license
 name is a known SPDX identifier and to obtain its longer description, a link
 to the full license text online and other meta-information about the license.
